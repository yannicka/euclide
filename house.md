- Le passage à un endroit modifie la zone actuelle du joueur (typiquement les toilettes).

- Le passage dans une pièce modifie d'autres zones (typiquement la maison).

- Certaines interactions ont un impact long terme (typiquement tuer le chien).

- Certaines toutes petites choses changent (typiquement la console de jeu : ps1 -> xbox -> ...)

- Des cartes étranges

- Faire qu'on se déplace de lieux en lieux différents (qu'on voit d'autres lieux, cf. espaces non-euclidiens)

- Des inceptions : on sort d'une pièce, et on se retrouve dans la même pièce (et si on a modifié un truc dans une pièce, ça fait pareil dans l'autre pièces identique ; cf. https://www.youtube.com/watch?v=vlhKOKsgbOo à 1 min 37)

Avec une vue de dessus, il n'est pas possible de modifier le contenu de la carte encore visible par le joueur. Il faut donc trouver une autre méthode :

1. Limiter la taille du rendu, de sorte à pouvoir faire ce qu'on veut en dehors.
2. Faire des salles clairement définies.
3. Faire en sorte que le joueur ne éclaire une zone, et on peut alors modifier en-dehors de la zone éclairée.

Aussi, il faut réfléchir au mode de déplacement, libre ou case par case.



Idée :
- Reprendre une base comme le jeu Creatures (monde 24x16)
- Déplacement case par case
- Monde ouvert
- On voit toujours au-delà ; le personnage ne s'approche jamais du bord de la carte
- Quand on entre dans une pièce, le joueur reste toujours positionné au centre du jeu



Monde non-euclidien :
- https://www.youtube.com/watch?v=kEB11PQ9Eo8
- https://www.youtube.com/watch?v=lFEIUcXCEvI
- https://www.youtube.com/watch?v=d-OxkJTAxO8
- https://www.youtube.com/watch?v=yqUv2JO2BCs
- https://www.youtube.com/watch?v=ag3vS3Lc8Ds
- https://www.youtube.com/watch?v=vlhKOKsgbOo
- https://www.youtube.com/watch?v=DDsiiC10KLg

Inspirations (si possible) :
- https://en.wikipedia.org/wiki/Liminal_space_(aesthetic)
- https://fr.wikipedia.org/wiki/The_Backrooms
