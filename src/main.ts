import { Keyboard } from './keyboard'
import { map } from './map'
import { Player } from './player'

const CELL_SIZE = 32

let seed = 1
function random() {
  const x = Math.sin(seed) * 10000
  return x - Math.floor(x)
}

function intersect(rect1: any, rect2: any) {
  return (
    rect1.x < rect2.x + rect2.width &&
    rect1.x + rect1.width > rect2.x &&
    rect1.y < rect2.y + rect2.height &&
    rect1.y + rect1.height > rect2.y
  )
}

const SCREEN_WIDTH = 24 * CELL_SIZE
const SCREEN_HEIGHT = 16 * CELL_SIZE

/** @ts-ignore */
let screen: Image|null = null
let time = 0

const keyboard = new Keyboard()

const player = new Player()

function cloneCanvas(oldCanvas: HTMLCanvasElement) {
  //create a new canvas
  var newCanvas = document.createElement('canvas') as HTMLCanvasElement;
  var context = newCanvas.getContext('2d') as CanvasRenderingContext2D;

  //set dimensions
  newCanvas.width = oldCanvas.width;
  newCanvas.height = oldCanvas.height;

  //apply the old canvas to the new one
  context.drawImage(oldCanvas, 0, 0);

  //return the new canvas
  return newCanvas;
}


const portalsOrigin = [
  {
    x: 8,
    y: 1,
    width: 1,
    height: 1,
    action: () => {
      for (let i = 1 ; i < 9 ; i++) {
        map[8][i] = 2
        map[7][i] = 2
        map[i][1] = 2
      }

      portals = updatePortals([{
        x: 1,
        y: 8,
        width: 1,
        height: 1,
        action: () => {
          for (let i = 1 ; i < 9 ; i++) {
            map[1][i] = 2
          }
        },
      }])
    },
  },
  {
    x: 11,
    y: 1,
    width: 1,
    height: 3,
    action: () => {
      // player.x -= speed

      // return

      time = 0.2

      screen = cloneCanvas(canvas)

      // map = houseMap

      player.x = 0 * CELL_SIZE
      player.y = 2 * CELL_SIZE + 4

      portals = updatePortals([
        {
          x: -1,
          y: 2,
          width: 1,
          height: 1,
          action: () => {
            time = 0.2
      
            screen = cloneCanvas(canvas)
      
            // map = mainMap
      
            player.x = 11 * CELL_SIZE
            player.y = 2 * CELL_SIZE + 4
      
            portals = updatePortals([])
          },
        },
      ])
    },
  },
]

let portals = updatePortals(portalsOrigin)

function updatePortals(portals: Array<any>) {
  return portals.map((portal: any) => {
    return {
      x: portal.x * CELL_SIZE,
      y: portal.y * CELL_SIZE,
      width: portal.width * CELL_SIZE,
      height: portal.height * CELL_SIZE,
      action: portal.action,
    }
  })
}

const canvas = document.querySelector('#game') as HTMLCanvasElement
const ctx = canvas.getContext('2d') as CanvasRenderingContext2D

canvas.width = SCREEN_WIDTH
canvas.height = SCREEN_HEIGHT

let x = 0

let houseMap = [
  [ 1, 1, 1, 1, 1, 1, 1, 1 ],
  [ 1, 0, 0, 0, 0, 0, 0, 1 ],
  [ 0, 0, 0, 0, 0, 0, 0, 1 ],
  [ 1, 0, 0, 0, 0, 0, 0, 1 ],
  [ 1, 0, 0, 0, 0, 0, 0, 1 ],
  [ 1, 1, 0, 1, 1, 1, 1, 1 ],
]

let mainMap = [
  [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
  [ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 ],
  [ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 ],
  [ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 ],
  [ 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1 ],
  [ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 ],
  [ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 ],
  [ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 ],
  [ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 ],
  [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
]

let lastUpdate = Date.now()

const speed = 200

function update() {
  const now = Date.now()
  const dt = (now - lastUpdate) / 1000
  lastUpdate = now

  if (keyboard.down('ArrowUp')) {
    player.dy = -0.04
  }

  if (keyboard.down('ArrowDown')) {
    player.dy = 0.04
  }

  if (keyboard.down('ArrowLeft')) {
    player.dx = -0.04
  }

  if (keyboard.down('ArrowRight')) {
    player.dx = 0.04
  }

  player.update()

  for (const portal of portals) {
    if (intersect(player, portal)) {
      portal.action()
    }
  }

  render(dt)

  window.requestAnimationFrame(update)
}

function render(dt: number) {
  ctx.fillStyle = 'rgb(244, 244, 244)'
  ctx.fillRect(0, 0, canvas.width, canvas.height)

  ctx.save()
  ctx.translate(Math.round(-player.x + SCREEN_WIDTH/2), Math.round(-player.y + SCREEN_HEIGHT/2))

  ctx.fillStyle = 'rgb(0, 0, 0)'

  for (let y = 0 ; y < map.length ; y ++) {
    for (let x = 0 ; x < map[y].length ; x ++) {
      if (map[y][x] === 0) {
        seed = x * y
        const val = Math.round(random() * 20 + 235)
        ctx.fillStyle = `rgb(${val}, ${val}, ${val})`
      } else if (map[y][x] === 1) {
        ctx.fillStyle = 'rgb(0, 0, 0)'
      } else if (map[y][x] === 2) {
        ctx.fillStyle = 'rgb(255, 0, 0)'
      }

      ctx.fillRect(x * CELL_SIZE, y * CELL_SIZE, CELL_SIZE, CELL_SIZE)
    }
  }

  ctx.fillStyle = 'rgba(255, 0, 255, 0.4)'

  for (const portal of portals) {
    ctx.fillRect(portal.x, portal.y, portal.width, portal.height)
  }

  player.render(ctx)

  ctx.restore()

  if (screen) {
    time -= dt

    if (time <= 0) {
      screen = null
    } else {
      ctx.globalAlpha = time / 0.2
      ctx.drawImage(screen, 0, 0)
      ctx.globalAlpha = 1
    }
  }
}

update()
