import { map } from "./map";

export function hasCollision(cx: number, cy: number): boolean {
  return typeof map[cy] !== 'undefined' && typeof map[cy][cx] !== 'undefined' && map[cy][cx] === 1
}
