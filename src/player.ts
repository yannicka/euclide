import { hasCollision } from "./collision"

export class Player {
  public width: number
  public height: number

  // Base coordinates
  public cx: number
  public cy: number
  public xr: number
  public yr: number

  // Resulting coordinates
  public xx: number
  public yy: number

  // Movements
  public dx: number
  public dy: number

  constructor() {
    this.width = 24
    this.height = 24

    this.cx = 2
    this.cy = 2
    this.xr = 0
    this.yr = 0
    this.xx = 0
    this.yy = 0
    this.dx = 0
    this.dy = 0
  }

  update() {
    // X
    this.xr+=this.dx;
    this.dx*=0.96;
    if( hasCollision(this.cx+1,this.cy) && this.xr>=0.7 ) {
      this.xr = 0.7;
      this.dx = 0;
    }
    if( hasCollision(this.cx-1,this.cy) && this.xr<=0.3 ) {
      this.xr = 0.3;
      this.dx = 0;
    }

    while (this.xr > 1) {	this.xr--; this.cx++; }
    while (this.xr < 0) {	this.xr++; this.cx--; }

    // Y
    this.yr+=this.dy;
    // this.dy+=0.05;
    this.dy*=0.96;

    if( hasCollision(this.cx,this.cy-1) && this.yr<=0.3 ) {
      this.dy = 0;
      this.yr = 0.3;
    }
    if( hasCollision(this.cx,this.cy+1) && this.yr>=0.7 ) {
      this.dy = 0;
      this.yr = 0.7;
    }

    while( this.yr>1 ) { this.cy++;this. yr--;}
    while( this.yr<0 ) {	this.cy--; this.yr++;}

    this.xx = Math.floor((this.cx + this.xr) * 32);
    this.yy = Math.floor((this.cy + this.yr) * 32);
  }

  public render(ctx: CanvasRenderingContext2D) {
    ctx.fillStyle = 'rgb(0, 0, 200)'
    ctx.fillRect(this.xx - this.width/2, this.yy - this.height/2, this.width, this.height)
  }

  public setCoordinates(x: number, y: number) {	
    this.xx = x
    this.yy = y
    this.cx = Math.floor(this.xx / 32)
    this.cy = Math.floor(this.yy / 32)
    this.xr = (this.xx - this.cx * 32) / 32
    this.yr = (this.yy - this.cy * 32) / 32
  }
}
